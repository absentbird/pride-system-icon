package main

import (
    "fyne.io/systray"
)

func setoptions() {
    items := map[string]*systray.MenuItem{}
    for _, i := range icons {
        items[i.name] = systray.AddMenuItem(i.menu, "Change System Icon")
        items[i.name].SetIcon(i.data)
        items[i.name].Enable()
        go func () {
            for {
                <-items[i.name].ClickedCh
                seticon(i.name)
            }
        }()
    }
}
