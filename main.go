package main

import (
	"flag"
	"fyne.io/systray"
	"github.com/kardianos/service"
	"io/ioutil"
	"log"
	"os"
    "path/filepath"
    "strings"
)

const (
    CONFIGDIR  = "prideicon"
    CONFIGFILE = "lastselected"
)

var logger service.Logger
var confpath string

type Icon struct {
    name string
    menu string
    data []byte
}

type program struct {
	exit chan struct{}
}

func check(e error, m string) {
	if e != nil {
		log.Panic(m+": ", e.Error())
	}
}

func seticon(path string) {
    var activeicon []byte
    var found bool
    for _, i := range icons {
        if i.name != path {
            continue
        }
        activeicon = i.data
        found = true
        break
    }
    if !found {
        var err error
        path = strings.TrimSpace(path)
        activeicon, err = ioutil.ReadFile(path)
        check(err, "Error opening icon")
    }
	systray.SetIcon(activeicon)
    ioutil.WriteFile(confpath, []byte(path), 0644)
    logger.Info("Changed icon: " + path)
}

func (p *program) Start(s service.Service) error {
	if service.Interactive() {
		logger.Info("Running in terminal.")
	} else {
		logger.Info("Running under service manager.")
	}
	p.exit = make(chan struct{})
	go p.run()
	return nil
}
func (p *program) run() error {
    systray.Run(onReady, onExit)
	return nil
}
func (p *program) Stop(s service.Service) error {
	close(p.exit)
	return nil
}

func onReady() {
	systray.SetTitle("Pride System Icon")
	systray.SetTooltip("System tray decoration.")
    setoptions()
	remove := systray.AddMenuItem("Remove Icon", "Exit Pride System Icon")
	remove.Enable()
	go func() {
		<-remove.ClickedCh
		systray.Quit()
        logger.Info("Closed by user")
	}()
    lastselected, err := ioutil.ReadFile(confpath)
    if err == nil {
        seticon(string(lastselected))
    } else {
        seticon("rainbow")
    }
}

func onExit() {
	os.Exit(0)
}

func main() {
    homedir, err := os.UserHomeDir()
    check(err, "Error finding home directory")
    confpath = filepath.Join(homedir, ".config", CONFIGDIR)
    err = os.MkdirAll(confpath, os.ModePerm)
    confpath = filepath.Join(confpath, CONFIGFILE)
    check(err, "Error creating config directory")
	svcFlag := flag.String("service", "", "Control the system service.")
	flag.Parse()
	options := make(service.KeyValue)
	options["SuccessExitStatus"] = "1 2 8 SIGKILL"
	svcConfig := &service.Config{
		Name:         "PrideIcon",
		DisplayName:  "Pride Tray Icon",
		Description:  "Creates a pride icon in system tray.",
		Dependencies: []string{},
		Option:       options,
	}
	prg := &program{}
	s, err := service.New(prg, svcConfig)
	check(err, "Error creating service")
	errs := make(chan error, 5)
	logger, err = s.Logger(errs)
	check(err, "Error logging channel")
	go func() {
		for {
			err := <-errs
			if err != nil {
				log.Print(err)
			}
		}
	}()
	if len(*svcFlag) != 0 {
		err := service.Control(s, *svcFlag)
		check(err, "Invalid action")
		return
	}
	err = s.Run()
	if err != nil {
		logger.Error(err)
	}
}
