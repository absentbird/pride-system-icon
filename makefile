BINARY_NAME=prideicon

all: build deploy clean

build:
	GOARCH=amd64 GOOS=linux go build -mod vendor -tags linux -o ${BINARY_NAME}-linux-amd64
	GOARCH=amd64 GOOS=windows go build -mod vendor -tags windows -ldflags -H=windowsgui -o ${BINARY_NAME}-windows-amd64

deploy:
	cp ${BINARY_NAME}-linux-amd64 dist/linux/${BINARY_NAME}
	cp install.sh dist/linux/
	cp ${BINARY_NAME}-windows-amd64 dist/windows/${BINARY_NAME}.exe
	cp install.bat dist/windows/

clean:
	go clean
	rm ${BINARY_NAME}-linux-amd64
	rm ${BINARY_NAME}-windows-amd64
