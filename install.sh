#!/bin/bash
echo "Installing Pride System Icon"
if [ ! -f prideicon ]; then
    echo "prideicon executable not found"
    exit 1
fi
WRITE=1
if [ -f $HOME/.local/bin/prideicon ]; then
    echo "~/.local/bin/prideicon already exists."
    read -r -p "Replace ~/.local/bin/prideicon? [Y/n] " response
    if [[ "$response" =~ ^([nN][oO]|[nN])$ ]]; then
        echo "File not replaced"
        WRITE=0
    fi
fi
if [ $WRITE = 1 ]; then
    [ ! -d ~/.local/bin ] && mkdir ~/.local/bin
    cp prideicon $HOME/.local/bin/prideicon
    echo "Copied executable to ~/.local/bin/prideicon"
fi
WRITE=1
if [ -f $HOME/.config/autostart/prideicon.desktop ]; then
    echo "~/.config/autostart/prideicon.desktop already exists."
    read -r -p "Replace ~/.config/autostart/prideicon.desktop? [Y/n] " response
    if [[ "$response" =~ ^([nN][oO]|[nN])$ ]]; then
        echo "File not replaced"
        WRITE=0
    fi
fi
if [ $WRITE = 1 ]; then
    [ ! -d ~/.config/autostart ] && mkdir ~/.config/autostart
    cat >~/.config/autostart/prideicon.desktop <<EOL
[Desktop Entry]
Name=PrideIcon
GenericName=Pride System Icon
Comment=Adds a pride icon to the system tray.
Exec=prideicon
Terminal=false
Type=Application
X-GNOME-Autostart-enabled=true
EOL
    echo "Added prideicon.desktop to ~/.config/autostart directory"
fi
echo "Starting program"
~/.local/bin/prideicon > /dev/null 2>&1 &
