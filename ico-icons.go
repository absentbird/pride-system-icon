//go:build windows
package main

import _ "embed"

//go:embed icons/pride.ico
var rainbow []byte
//go:embed icons/progress.ico
var progress []byte

var icons []Icon = []Icon{
    Icon{
        name: "rainbow",
        menu: "Rainbow",
        data: rainbow,
    },
    Icon{
        name: "progress",
        menu: "Progress",
        data: progress,
    },
}
