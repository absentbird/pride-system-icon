# Pride Flag App Indicator

Add a pride flag to the system tray.

Compiled files are located in the `dist/[os]` directory of this repository.

Run `prideicon` on Linux or `prideicon.exe` on Windows to add the icon to the systemtray.

Click the icon to change the flag or remove the icon from the system tray.

### Autostart on Linux

Run `install.sh` to copy the `prideicon` executable to `~/.local/bin/prideicon` and add `prideicon.desktop` to `~/.config/autostart`, causing the program to run automatically on login.

### Autostart on Windows

Run `install.bat` to copy `prideicon.exe` into: `%APPDATA%\Microsoft\Windows\Start Menu\Programs\Startup` causing the program to run automatically at startup.

### Compiling
Run `make` while in the repository's top directory (on Windows you may need to install GNU make).

Go must be installed to compile the program: https://go.dev/doc/install

The compiled files will be placed in `dist/windows/prideicon.exe` and `dist/linux/prideicon`

Commands for compiling without using make:

**Linux**:  
`go build -mod vendor -tags linux -o prideicon`

**Windows 10+**:  
`go build -mod vendor -tags windows -ldflags -H=windowsgui -o prideicon.exe`

#### Credit:
* [Icons created by Trazobanana](https://www.flaticon.com/packs/lgbt-flags)
* ['Progress' pride flag created by Daniel Quasar](https://progress.gay/)
