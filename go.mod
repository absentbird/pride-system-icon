module gitlab.com/absentbird/pride-system-icon

go 1.22.4

require (
	fyne.io/systray v1.11.0
	github.com/kardianos/service v1.2.2
)

require (
	github.com/godbus/dbus/v5 v5.1.0 // indirect
	golang.org/dl v0.0.0-20240621154342-20a4bcbb3ee2 // indirect
	golang.org/x/sys v0.15.0 // indirect
)
