//go:build linux
package main

import _ "embed"

//go:embed icons/pride.png
var rainbow []byte
//go:embed icons/transgender.png
var trans []byte
//go:embed icons/non-binary.png
var nonbinary []byte
//go:embed icons/progress.png
var progress []byte

var icons []Icon = []Icon{
    Icon{
        name: "rainbow",
        menu: "Rainbow",
        data: rainbow,
    },
    Icon{
        name: "trans",
        menu: "Trans",
        data: trans,
    },
    Icon{
        name: "nonbinary",
        menu: "Non-Binary",
        data: nonbinary,
    },
    Icon{
        name: "progress",
        menu: "Progress",
        data: progress,
    },
}
